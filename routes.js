const express = require('express');
let dataController = require('./dataController');
let router = express.Router();
// Set default API response

DataSense = require('./dataSenseModel');

router.get('/dataSense/index', dataController.index);
router.get('/dataSense', dataController.findAll);

router.get('/script', startScript);
function startScript(req, res) {
    var spawn = require("child_process").spawn;
    var process = spawn('python3', ["./script.py"]);
}

router.post('/dataSense', async function(req, res) {
    let status = {
        status: 200,
        message: 'New dataSense created !'
    };
    try {
        let dataSense = new DataSense({
            temperature: req.body.temperature,
            humidity: req.body.humidity,
            pression: req.body.pression,
            createdAt: Date()
        });
        console.log(dataSense);
// save the data and check for errors
        dataSense.save();
    } catch (error) {
        status.status = 400;
        status.message = error.name + ': ' + error.message
    }
    await res.status(status.status).send(status)
});
router.get('/dataSense/:id', dataController.findOne);
router.put('/dataSense/:id', dataController.update);
router.delete('/dataSense/:id', dataController.delete);


// Export API routes
module.exports = router;