// import express JS module into app
// DEPRECATED
let apiRoutes = require("./routes")
let bodyParser = require('body-parser');
let mongoose = require('mongoose');

// and creates its variable. 
var express = require('express'); 
var app = express(); 

app.use('/api', apiRoutes)
app.use(bodyParser.json());
app.get('/', home);

function home(req, res) {
    res.send('home');
}

// Function startScript() is executed 
app.get('/script', startScript); 

function startScript(req, res) { 
	
	// Use child_process.spawn method from 
	// child_process module and assign it 
	// to variable spawn 
	var spawn = require("child_process").spawn; 
	
	// Parameters passed in spawn - 
	// 1. type_of_script 
	// 2. list containing Path of the script 
	// and arguments for the script 
	
	// E.g : http://localhost:3000/name?firstname=Mike&lastname=Will 
	// so, first name = Mike and last name = Will 
	var process = spawn('python3',["./script.py"]); 
} 

// save code as start.js 
// Creates a server which runs on port 3000 and                                 
// can be accessed through localhost:3000                                       
app.listen(3000, function() {                                                   
        console.log('server running on port 3000');                             
})

mongoose.connect('mongodb://localhost/resthub', { useNewUrlParser: true});
var db = mongoose.connection;

