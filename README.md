# Embedded System Application Development (BJTU)

Application developed by **Arnaud Jabiol** & **Hugo Dalger** for BJTU Embedded System Application Development module.

It is an application that allows you to switch on, depending on the temperature, humidity and pressure. The higher these values, the higher the number of LEDs lit.

### Requirements

 - Node.js
 - Mongo
 - Sense Hat Emulator

### Installation


Be sure that MongoDb is installed and service is launched

```sh
$ sudo systemctl start mongod.service
```

Install the dependencies and start the server.

```sh
$ cd EmbeddedSystem1
$ npm install -d
$ node index.js 
```
After this you'll get the home page on port **8080**

![home](https://gitlab.com/ArnaudJabJab/embeddedsystem1/-/raw/master/screens/home.png)

### Functionnalities

You're free to define the temperature / humidity / pressure when clicking **Start the program**.
And show how the Led's shine depending of the value.

![home](https://gitlab.com/ArnaudJabJab/embeddedsystem1/-/raw/master/screens/emulator.png)


you can find the data you have entered by pressing the **Get Data** button

![home](https://gitlab.com/ArnaudJabJab/embeddedsystem1/-/raw/master/screens/getbtn.png)

You can follow these values over the long term by entering these values weekly for example