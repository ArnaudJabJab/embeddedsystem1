let mongoose = require('mongoose');
// Setup schema

    const dataSenseSchema = new mongoose.Schema({
        temperature: Number,
        humidity: Number,
        pression: Number,
        createdAt: Date
    });
// Export Contact model
module.exports = mongoose.model('DataSense', dataSenseSchema);